use std::str::from_utf8;
use http::{
  Method,
  Version,
  header::{
    HeaderMap,
    HeaderName,
    HeaderValue
  }
};

pub fn parse_first_line(buffer: &[u8]) -> (
  Option<Method>,
  Option<Version>,
  Option<&str>, u32
) {

  let mut http_method = None;
  let mut http_path = None;
  let mut version = None;

  let mut previous_cursor = 0usize;
  let mut method_parsed = false;
  let mut path_parsed = false;
  let mut protocol_version_parsed = false;

  let mut cariage_met = false;

  let mut cursor = 0;
  while cursor < buffer.len() {
    let byte = buffer[cursor];

    if byte == ('\r' as u8) {
      cariage_met = true;
      cursor += 1;
      continue;
    }
    else if byte != ('\n' as u8) || !cariage_met {
      cariage_met = false;
    }
    
    if (!cariage_met) && (byte != (' ' as u8) || cariage_met) {
      cursor += 1;
      continue;
    } else {
      if !method_parsed {
        method_parsed = true;
        http_method = match Method::from_bytes(&buffer[previous_cursor..cursor]) {
          Ok(method) => Some(method),
          Err(_) => None
        };
        previous_cursor = cursor+1;
      } else if !path_parsed {
        path_parsed = true;
        http_path = match from_utf8(&buffer[previous_cursor..cursor]) {
          Ok(path) => Some(path),
          Err(_) => None
        };
        previous_cursor = cursor+1;
      } else if !protocol_version_parsed {
        protocol_version_parsed = true;
        version = match &buffer[previous_cursor..(cursor-1)] {
          b"HTTP/0.9" => Some(Version::HTTP_09),
          b"HTTP/1.0" => Some(Version::HTTP_10),
          b"HTTP/1.1" => Some(Version::HTTP_11),
          b"HTTP/2.0" => Some(Version::HTTP_2),
          b"HTTP/2" => Some(Version::HTTP_2),
          b"HTTP/3.0" => Some(Version::HTTP_3),
          b"HTTP/3" => Some(Version::HTTP_3),
          _ => None
        };
        previous_cursor = cursor+1;
      }
    }

    cursor += 1;
    if cariage_met {
      break;
    }
  }

  (http_method, version, http_path, cursor as u32)
}

pub fn parse_headers(buffer: &[u8], start: u32) -> (Option<HeaderMap>, u32) {
  let mut headers = HeaderMap::new();
  let mut cariage_met = false;
  let mut previous_cursor = start as usize;
  let mut cursor = start as usize;

  let mut header_names: Vec<&[u8]> = vec![];
  let mut header_values: Vec<&[u8]> = vec![];
  let mut seperating_colon_met = false;

  loop {
    let byte = buffer[cursor];

    if byte == ('\r' as u8) {
      cariage_met = true;
      cursor += 1;
      continue;
    } else if byte == ('\n' as u8) && cariage_met {
      if header_names.len() == header_values.len() {
        cursor += 1;
        break;
      }
      seperating_colon_met = false;
      header_values.push(&buffer[previous_cursor..(cursor-1)]);
      previous_cursor = cursor + 1;
    } else {
      cariage_met = false;
    }

    if byte == (':' as u8) && !seperating_colon_met {
      seperating_colon_met = true;
      header_names.push(&buffer[previous_cursor..cursor]);
      previous_cursor = cursor + 1;
    } else if previous_cursor == cursor && byte == (' ' as u8) {
      previous_cursor = cursor + 1;
    }

    cursor += 1;
  }

  if header_names.len() != header_values.len() {
    return (None, 0);
  }

  for (name, value) in header_names.into_iter().zip(header_values.into_iter()) {
    let name = match HeaderName::from_bytes(name) {
      Ok(hn) => hn,
      Err(_) => return (None, 0)
    };
    
    let value = match HeaderValue::from_bytes(value) {
      Ok(hv) => hv,
      Err(_) => return (None, 0)
    };

    headers.insert(name, value);
  }  

  (Some(headers), cursor as u32)
}

pub fn parse_body(buffer: &[u8], start: u32) -> &[u8] {
  if buffer.len() < start as usize {
    return b"";
  }

  let start = start as usize;
  &buffer[start..(buffer.len())]
}

pub fn normalize_buffer(buffer: &[u8]) -> &[u8] {
  let mut end = buffer.len();
  for (index, byte) in buffer.iter().enumerate() {
    if *byte == 0 {
      end = index;
      break;
    }
  }
  &buffer[0..end]
}

#[cfg(test)]
mod tests {
  use http::{
    Method,
    Version
  };

  #[test]
  fn parse_headers_correctly() {
    let example_headers = b"Content-Type: application/json,text/csv\r\n\
                                    Host: www.bora-is-awesome.com\r\n\
                                    Accept: text/plain\r\n\
                                    content-type: application/json\r\n\r\n";

    let (headers, end) = super::parse_headers(example_headers, 0);
    assert_ne!(headers, None);
    let headers = headers.unwrap();
    assert_eq!(headers.get("Content-Type").unwrap(), &"application/json");
    assert_eq!(headers.get("Host").unwrap(), &"www.bora-is-awesome.com");
    assert_eq!(headers.get("Accept").unwrap(), &"text/plain");
    assert_eq!(end, example_headers.len() as u32);
  }

  #[test]
  fn first_line_parsed_successfuly() {
    let requests: Vec<&[u8]> = vec![
      b"GET / HTTP/1.1\r\n",
      b"POST /bora/is/awesome HTTP/1.0\r\n",
      b"PUT /bora/is/awesome HTTP/2.0\r\n",
      b"DELETE /with-query-string?query=string HTTP/1.1\r\n"
    ];

    let equivalents = vec![
      (Method::GET, Version::HTTP_11, "/", 16),
      (Method::POST, Version::HTTP_10, "/bora/is/awesome", 32),
      (Method::PUT, Version::HTTP_2, "/bora/is/awesome", 31),
      (Method::DELETE, Version::HTTP_11, "/with-query-string?query=string", 49)
    ];

    for (index, req) in requests.iter().enumerate() {
      let extract = super::parse_first_line(req);
      assert_eq!(extract.0.unwrap(), equivalents.get(index).unwrap().0);
    }
  }

  #[test]
  fn parse_header_values_that_has_colon_character() {
    let example_headers = b"Content-Type: application/json,text/csv\r\n\
                                    Host: localhost:9898\r\n\
                                    Accept: text/html\r\n\r\n";
    
    let (headers, end) = super::parse_headers(example_headers, 0);
    assert_ne!(headers, None);

    let headers = headers.unwrap();
    assert_eq!(headers.get("Content-Type").unwrap(), &"application/json,text/csv");
    assert_eq!(headers.get("Host").unwrap(), &"localhost:9898");
    assert_eq!(headers.get("accept").unwrap(), &"text/html");
    assert_eq!(end, example_headers.len() as u32);
  }

  #[test]
  fn buffer_normalization_works_correctly_when_there_are_null_terminated_characters() {
    let example_headers = b"Content-Type: application/json,text/csv\r\n\
                                    Host: localhost:9898\r\n\
                                    Accept: text/html\r\n\r\n";

    let mut big_buffer = [0u8; 1024];
    for (i, b) in example_headers.iter().enumerate() {
      big_buffer[i] = *b;
    }

    let buffer = super::normalize_buffer(&big_buffer);
    assert_eq!(buffer.len(), example_headers.len());
  }

  #[test]
  fn buffer_normalization_works_correctly_when_there_arent_null_terminated_characters() {
    let example_headers = b"Content-Type: application/json,text/csv\r\n\
                                    Host: localhost:9898\r\n\
                                    Accept: text/html\r\n\r\n";
    let buffer = super::normalize_buffer(example_headers);
    assert_eq!(buffer.len(), example_headers.len());
  }

  #[test]
  fn buffer_normalization_works_correctly_when_no_content() {
    let example_headers = b"";
    let buffer = super::normalize_buffer(example_headers);
    assert_eq!(example_headers.len(), buffer.len());
  }
}
