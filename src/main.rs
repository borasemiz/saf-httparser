use std::net::{TcpStream, TcpListener};
use std::io::prelude::*;

fn main() {
  let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
  println!("Listening...");

  for client in listener.incoming() {
    let mut stream = client.unwrap();
    let mut buffer = [0; 256];
    stream.read(&mut buffer).unwrap();

    println!("{:?}\n{} {:?}", &buffer, &buffer.len(), '\0' as u8 == 0);
  }
}