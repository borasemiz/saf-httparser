# SAF HTTP Parser

A very simple Rust library for parsing HTTP messages for simple people.

## Usage
You use this library in combination with [http](https://crates.io/crates/http) crate because it depend on the types that are defined in there. Make sure that this crate is added in your `Cargo.toml`.

```toml
[dependencies]
http = "*"
saf-httparser = "*"
```

This library is used for only one thing. You have a byte content of an HTTP message and you want to parse that to a `http::Request`. The library exposes only two functions for you to use.

```rust
use saf_httparser::request_from_bytes;
use http::Version;

let example_request = b"GET /somepath HTTP/1.1\r\n\
                        Host:www.somehost.com\r\n\
                        Content-Type:text/plain\r\n\r\nan awesome message";

let request: Request<&[u8]> = request_from_bytes(example_request).unwrap();

assert_eq!(request.method().as_str(), "GET");
assert_eq!(request.uri().path(), "/somepath");
assert_eq!(request.version(), Version::HTTP_11);
assert_eq!(request.headers().get("host").unwrap(), &"www.somehost.com");
assert_eq!(request.headers().get("content-type").unwrap(), &"text/plain");
assert_eq!(request.body(), b"an awesome message");
```

This is a bare minimum HTTP message parsing library. It parses every HTTP message that conforms with the HTTP message structure speficication. In case the message doesn't conform with the correct structure, it will return a `String` wrapped in an `Err` describing what went wrong.